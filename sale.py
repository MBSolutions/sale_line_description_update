#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class SaleLine(ModelSQL, ModelView):
    "Sale Line"
    _name = "sale.line"

    def on_change_product(self, vals):
        party_obj = Pool().get('party.party')
        product_obj = Pool().get('product.product')

        res = super(SaleLine, self).on_change_product(vals)

        context = {}
        if not vals.get('product'):
            return res

        if vals.get('_parent_sale.party'):
            party = party_obj.browse(vals['_parent_sale.party'])
            if party.lang:
                context['language'] = party.lang.code

        product = product_obj.browse(vals['product'])

        with Transaction().set_context(**context):
            res['description'] = product_obj.browse(product.id).name

        return res

SaleLine()

