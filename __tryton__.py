#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Sale Line Description Update',
    'name_de_DE': 'Verkaufsposition Aktualisierung Beschreibung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Updates the description of a sale line on change of a product
''',
    'description_de_DE': '''
    - Aktualisiert die Beschreibung einer Verkaufsposition, wenn das Produkt
      einer Position geändert wird.
''',
    'depends': [
        'sale_pricelist'
    ],
    'xml': [
    ],
    'translation': [
    ],
}
